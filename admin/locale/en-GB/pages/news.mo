��    4      �  G   \      x     y     �     �     �     �     �     �     �                         %     7     C  
   S     ^     l     x     �     �     �     �     �     �     �     �     �               &     3     G     [     l     z  
   �     �     �     �     �     �     �     �     �     �          0     H     Z     q  (   �     �  %   �  )   �  #     (   /  %   X     ~     �     �  	   �     �     �     �     �     �     �     �     	     	  
   &	     1	     A	     R	     g	     x	  
   �	  F   �	     �	     �	  y   �	     m
     t
     �
     �
  
   �
     �
     �
     �
     �
     �
            
   !  .   ,  !   [  ,   }  .   �  !   �  )   �  )   %     O     (              
            4                              ,          )   '      %       /      2                           #      3   "   1   $   .                  0   	                                            *   &             !   +   -           deleteComment errorAtLeastOneNews errorAtLeastOneNewsCat errorNewsCatFirst errorNewsCatNotFound errorNewsImageDelete goOnEdit mainCategory newAdd newEdit news newsActivate newsAlreadyExists newsArchive newsArchiveLast newsAuthor newsCatCreate newsCatEdit newsCatLastUpdate newsCatOverview newsCatParent newsCatSort newsCommentActivate newsCommentEdit newsComments newsDate newsDeleteCat newsDeleteComment newsDeleteNews newsDesc newsInfinite newsMandatoryFields newsMetaDescription newsMetaKeywords newsMetaTitle newsOverview newsPicAdd newsPics newsPraefix newsPreviewText newsSeo newsURL newsValidation successNewsCatDelete successNewsCatSave successNewsCommentDelete successNewsCommentUnlock successNewsCommmentEdit successNewsDelete successNewsImageDelete successNewsSave Content-Type: text/plain; charset=UTF-8
 Delete comments? Please select at least one blog post. Please select at least one blog category. Please enter a blog category first. Could not find blog category with ID %d. Could not delete selected blog image.  and continue editing Main category Create new post Edit post Blog Activate already exists. Archive Archived posts Author Create new category Edit category Last update Categories Parent category Category sorting Unactivated comments Editing comments Comments Created on The following blog categories and their subcategories will be deleted: Delete comment Delete blog post Use the JTL-Shop blog to keep your customers up to date. Create new blog posts and specify how posts are to be displayed. always (*) mandatory field Meta description Meta keywords Meta title Posts Add another image Available images Prefix for monthly overview Preview text SEO https://jtl-url.de/7vfnm Created on Selected blog categories deleted successfully. Blog category added successfully. Selected blog comments deleted successfully. Selected blog comments activated successfully. Blog comment edited successfully. Selected blog posts deleted successfully. Selected blog image deleted successfully. Blog post saved successfully. 