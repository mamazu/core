��    P      �  k         �  
   �     �     �               ,     ?     Q     f     u     �     �  
   �     �  
   �     �  
   �     �     �               +     7  	   E     O     [     h  	   x     �     �     �     �     �     �     �     �  	   �     �  	   �     �     	     	     )	     8	     G	     V	     c	     |	     �	     �	  	   �	     �	     �	     �	     �	     �	     �	     
     
     
     )
  
   6
     A
     P
     ^
     o
     �
     �
     �
     �
     �
     �
     �
               4     F     Y     s  (   �     �     �     �     �     
           2     K     `     p     �     �     �     �  
   �     �  
   �     �     �            	   5     ?     M     Z  	   c     m     }  	   �     �     �     �     �     �     �     �  !  �     �       �     \   �     "     ;     K     ^     t  S   �     �     �       
              (     6  0   H  1   y     �     �     �     �     �     �     �                8     W     p     �     �     �     �  	   �     �     �     �     �  w     )   �     =              H       !      $   "   5      J       I       O   '   G       6   8      L      K   :             F      C      2          ,   ?   @          M      (       3   B   A      0   >          +       .         P      1                  )         D              #             9                        *       -               N   E      4   7   /                  	   &   <   ;   %   
    Bestseller Eigene Box (Mit Rahmen) Eigene Box (Ohne Rahmen) Filter (Bewertung) Filter (Hersteller) Filter (Kategorie) Filter (Merkmale) Filter (Preisspanne) Filter (Suche) Filter (Suchspecial) Filter (Tag) Globale Merkmale Hersteller In Kürze Verfügbar Kategorien Konfigurator Linkgruppe Login Neue im Sortiment News (Monatsübersicht) News Kategorien Schnellkauf Sonderangebot Suchwolke Top Angebot Top bewertet Vergleichsliste Warenkorb Wunschliste Zuletzt angesehen boxEdit boxLabel boxTemplate boxTitle boxType boxen boxenDesc boxenURL catBoxNum catBoxNumTooltip confirmDeleteBox deactivatedOnAllPages deleteSelected errorBoxCreate errorBoxDelete errorBoxEdit errorBoxesVisibilityEdit errorContainerCreate inContainer invisibleBoxes linkgroup new newContainer noBoxActivated noBoxesAvailableFor noTemplateConfig pleaseSelect remove sectionBottom sectionLeft sectionRight sectionTop showBoxOnlyFor showContainer successBoxCreate successBoxDelete successBoxEdit successBoxRefresh successContainerCreate templateTypeCategory templateTypeContent templateTypeExtension templateTypeLinkList templateTypePlugin templateTypeTemplate visibleOnAllPages visibleOnSomePages warningChangesForAllPages warningInvisibleBoxes Content-Type: text/plain; charset=UTF-8
 Bestsellers Custom box (with frame) Custom box (without frame) Filter (customer review) Filter (manufacturer) Filter (category) Filter (characteristics) Filter (price range) Filter (search) Filter (item badge) Filter (tag) Global characteristics Manufacturer Available soon Categories Configurator Link group Login New in product range Blog: months with blog posts Blog: blog categories Quick buy Special offer Search cloud Top item Top rated Comparison list Basket Wish list Recently viewed Edit box Description Template Name Type Footers/Boxes Here you can create and edit boxes for different box containers and manage the footer. You can specify for each container on which pages it should be visible. Depending on the template used, different containers are available. Some boxes can also be further configured via the Edit button. https://jtl-url.de/li846 Category box ID Only lists categories with the category attribute "category box" and the set number. For all categories with or without a function attribute, the default value is 0. Delete box "%s"? If you delete a container, all boxes in the container will also be deleted. deactivated on all pages Delete selected Could not add box. Could not remove box. Could not edit box. Could not set box visibility due to a MySQL error. Please contact the Support team. Could not create container. In container Invisible boxes Link group New box New container No box activated. Currently there are no boxes available for "%s". Could not find configuration file (template.xml). Please select Remove from list Footer Left side bar Right side bar Header Show box "%s" only for: Activate container Box added successfully.  Box(es) deleted successfully. Box successfully edited. Boxes updated successfully. Container created successfully. Category Contents Plug-in Link list Plug-in Template visible on all pages visible on all selected pages The selection "All pages" overwrites existing changes to the boxes that were performed for individual pages beforehand. Some boxes cannot currently be displayed. 